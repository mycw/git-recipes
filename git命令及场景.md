### 初始化一个远程仓库
1. 如果远程和本地项目都不存在(即github上只创建了一个空仓库)
```
mkdir projectName
cd projectName
echo "README" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin url/account/project.git
git push -u origin master
```
2. 如果本地已经存在项目
```
cd projectName
git init
git add .
git commit -m "upload local project"
git remote add origin url/accout/project.git
git push -u origin master
```
3. 如果添加远程分支出错
```
git remote remove origin url/accout/project.git
git remote add origin url/accout/project.git
```

---
### 下载已有项目到本地
```
cd workspace
git clone url/accout/project.git
cd projectName
```

---
### 提交本地修改到远程仓库
1. 增加或修改文件后的提交
```
git add files
git commit -m "log remark"
git pull origin master
git push origin master
```
2. 上述过程出现冲突
```
git mergetool
git commit -m "merge log"
git push origin master
```
3. 删除本地文件后的提交
```
git rm files
git commit -m "remove files"
git pull origin master
git push origin master
```
4. 保留本地文件但是删除远程版本库文件
```
git rm --cached files
git commit -m "remove remote files"
git pull origin master
git push origin master
```
5.如果刚才提交时忘了暂存某些修改，可以先补上暂存操作，然后再运行 --amend 提交：
```
git commit -m 'initial commit'
git add forgotten_file
git commit --amend
```
---
### 合并远程分支到当前分支
1. 自动合并
```
git pull origin master
```
2. 半自动合并
```
git fetch origin master
git merge
```
- git merge也可带参数
```
git merge --ff-only #如果合并过程出现冲突，Git会自动abort此次merge。
git merge --no-ff #强行关闭fast-forward方式。可以保存你之前的分支历史。
git merge --squash #用来把一些不必要commit进行压缩
```
3. 合并时产生冲突
```
git mergetool #调用可视化工具多路合并,windows下可用beyondcompare，Linux下可用meld
git commit -m "merge log"
git push origin master
```

---
### 分支的创建与合并
1. 新建分支
```
git branch develop master    # 从master分支上新建develop分支
git checkout develop    # 检出develop分支
# 此处可进行功能开发，并add和commit到develop分支
git push origin develop    # 推送develop分支到远端的origin/develop
```

---
### 给某个历史版本编号
1.查看目前已有的版本编号
```
git tag 
```
2.给当前代码打新的版本号
```
git tag -a 0.1 -m "版本更新的具体描述" master
git push --tags
```
3.给某次历史提交的代码打版本







